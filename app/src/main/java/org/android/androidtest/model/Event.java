package org.android.androidtest.model;

/**
 * Created by Akantu-O'Kudi on 9/29/2016.
 */
import java.util.ArrayList;

public class Event {
    private String title, imageUrl;


    public Event() {
    }

    public Event(String name, String imageUrl) {
        this.title = name;
        this.imageUrl = imageUrl;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}