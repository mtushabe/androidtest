package org.android.androidtest.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.android.androidtest.R;
import org.android.androidtest.app.AppController;
import org.android.androidtest.model.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akantu-O'Kudi on 9/29/2016.
 */
public class ListAdapter extends BaseAdapter {


    private Activity activity;
    private LayoutInflater inflater;
    private List<Event> events;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public ListAdapter(Activity activity, List<Event>events){
        this.activity= activity;
        this.events = events;

    }

    @Override
    public int getCount() {
        return events.size();
    }

    @Override
    public Object getItem(int index) {
        return events.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View rowView, ViewGroup viewGroup) {

        if (inflater == null)
            inflater = activity.getLayoutInflater();
        if (rowView == null)
            rowView = inflater.inflate(R.layout.list_row, null);
        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView imageView = (NetworkImageView) rowView
                .findViewById(R.id.thumbnail);

        // this gets event data for the row from the event list
        Event event = events.get(position);

        // event image
        imageView.setImageUrl(event.getImageUrl(), imageLoader);
        //event title
        TextView title = (TextView) rowView.findViewById(R.id.title);

        title.setText(event.getTitle());


        return rowView;
    }
}
